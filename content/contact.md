+++
title = "Contact"
in_search_index = true
+++

## Legal information

SEGFAULT LABS s.r.o.

Plzenecká 222/22, Východní Předměstí, 326 00 Plzeň

IČ: 198 60 145

DIČ: CZ19860145

## Contact

Email: info@segfault-labs.dev

Phone: +420 602 807 773
